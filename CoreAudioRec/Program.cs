﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using Vannatech.CoreAudio.Constants;
using Vannatech.CoreAudio.Enumerations;
using Vannatech.CoreAudio.Externals;
using Vannatech.CoreAudio.Interfaces;
using Vannatech.CoreAudio.Structures;

namespace CoreAudioRec
{
	public static class Program
	{
		private static void printHelp()
		{
			Console.Error.WriteLine("Usage: coreaudiorec <mode> [deviceName]");
			Console.Error.WriteLine("Where <mode> is either \"render\" or \"capture\".");
			Console.Error.WriteLine("Optional: The device name is the display name of the audio device as found in the Windows Mixer.");
			Console.Error.WriteLine("The default device will be used if none is specified.");
		}

		public static int Main(string[] args)
		{
			if (args.Length < 1)
			{
				printHelp();
				return 1;
			}

			EDataFlow flow;

			if (args[0] == "render")
			{
				flow = EDataFlow.eRender;
			}
			else if (args[0] == "capture")
			{
				flow = EDataFlow.eCapture;
			}
			else
			{
				printHelp();
				return 1;
			}
			
			// enumerate devices...
			IMMDeviceEnumerator pEnumerator = (IMMDeviceEnumerator)Activator.CreateInstance( Type.GetTypeFromCLSID(new Guid(ComCLSIDs.MMDeviceEnumeratorCLSID)));
			IMMDeviceCollection pCollection;
			IMMDevice pDevice = null; 

			if (args.Length >= 2)
			{
				uint count;
				checkError(pEnumerator.EnumAudioEndpoints(flow, DEVICE_STATE_XXX.DEVICE_STATE_ACTIVE, out pCollection));
				checkError(pCollection.GetCount(out count));

				bool found = false;
				for (uint i = 0; i < count; i++)
				{
					checkError(pCollection.Item(i, out pDevice));

					if (getDisplayName(pDevice) == args[1])
					{
						found = true;
						break;
					}

					Marshal.FinalReleaseComObject(pDevice);
				}

				if (!found)
				{
					Console.Error.WriteLine("Device not found.");
					return 1;
				}
			}
			else
			{
				checkError(pEnumerator.GetDefaultAudioEndpoint(flow, ERole.eMultimedia, out pDevice));
			}

			string id;
			checkError(pDevice.GetId(out id));

			Console.Error.WriteLine("Using device {0} {1}", getDisplayName(pDevice), id);

			// open device...
			const ulong REFTIMES_PER_SEC = 10000000;
			const ulong REFTIMES_PER_MILLISEC = 10000;

			object instance;
			checkError(pDevice.Activate(new Guid(ComIIDs.IAudioClientIID), (uint)CLSCTX.CLSCTX_INPROC_HANDLER, IntPtr.Zero, out instance));
			IAudioClient pAudioClient = (IAudioClient)instance;

			IntPtr pwfxPtr;
			checkError(pAudioClient.GetMixFormat(out pwfxPtr));
			checkError(pAudioClient.Initialize(AUDCLNT_SHAREMODE.AUDCLNT_SHAREMODE_SHARED, flow == EDataFlow.eRender ? AUDCLNT_STREAMFLAGS_XXX.AUDCLNT_STREAMFLAGS_LOOPBACK : 0, REFTIMES_PER_SEC, 0, pwfxPtr, Guid.Empty));
			checkError(pAudioClient.GetService(new Guid(ComIIDs.IAudioCaptureClientIID), out instance));
			IAudioCaptureClient pCaptureClient = (IAudioCaptureClient)instance;
			checkError(pAudioClient.Start());

			WAVEFORMATEX pwfx = (WAVEFORMATEX)Marshal.PtrToStructure(pwfxPtr, typeof(WAVEFORMATEX));
			Console.Error.WriteLine("{0} channels, {1} bits per sample, {2} samples per second.", pwfx.nChannels, pwfx.wBitsPerSample, pwfx.nSamplesPerSec);
			
			uint bufferFrameCount;
			checkError(pAudioClient.GetBufferSize(out bufferFrameCount));

			double hnsActualDuration = (double)REFTIMES_PER_SEC * bufferFrameCount / pwfx.nSamplesPerSec;

			Stream stdout = Console.OpenStandardOutput();

			bool run = true;
			Console.CancelKeyPress += (sender, e) => { run = false; };
			while (run)
			{
				//Thread.Sleep((int)(hnsActualDuration / REFTIMES_PER_MILLISEC / 2));
				Thread.Sleep(1);

				uint packetLength;
				checkError(pCaptureClient.GetNextPacketSize(out packetLength));

				while (packetLength != 0)
				{
					// capture data.
					IntPtr pData;
					uint numFramesAvailable;
					AUDCLNT_BUFFERFLAGS flags;
					ulong devicePosition;
					ulong counterPosition;
					checkError(pCaptureClient.GetBuffer(out pData, out numFramesAvailable, out flags, out devicePosition, out counterPosition));

					// write to stdout.
					byte[] data = new byte[numFramesAvailable * pwfx.nChannels * (pwfx.wBitsPerSample / 8)];
					Marshal.Copy(pData, data, 0, data.Length);
					stdout.Write(data, 0, data.Length);

					//Console.Error.WriteLine("frames: {0}, packetLength: {1}, dataLength: {2}, devPos: {3}, counterPos: {4}", numFramesAvailable, packetLength, data.Length, devicePosition, counterPosition);
					
					checkError(pCaptureClient.ReleaseBuffer(numFramesAvailable));
					checkError(pCaptureClient.GetNextPacketSize(out packetLength));
				}
			}

			checkError(pAudioClient.Stop());

			// close

			Marshal.FreeCoTaskMem(pwfxPtr);
			Marshal.FinalReleaseComObject(pEnumerator);
			Marshal.FinalReleaseComObject(pDevice);
			Marshal.FinalReleaseComObject(pAudioClient);
			Marshal.FinalReleaseComObject(pCaptureClient);

			return 0;
		}

		private static void checkError(int errorCode)
		{
			if (errorCode != 0 && errorCode != 0x08890001)
				throw new CoreAudioException(unchecked((uint)errorCode));
		}

		private static string getDisplayName(IMMDevice device)
		{
			IPropertyStore properties;
			checkError(device.OpenPropertyStore(STGM.STGM_READ, out properties));

			PROPERTYKEY key;
			key.fmtid = PropertyKeys.PKEY_Device_FriendlyName;
			key.pid = 2;
			PROPVARIANT variant;
			checkError(properties.GetValue(ref key, out variant));

			string displayName = Marshal.PtrToStringAuto(variant.Data.AsStringPtr);

			Marshal.FinalReleaseComObject(properties);

			return displayName;
		}
	}
}
