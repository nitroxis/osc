﻿using System.Runtime.InteropServices;

namespace Vannatech.CoreAudio.Structures
{
	/// <summary>
	/// Stores the video port identifier.
	/// </summary>
	/// <remarks>
	/// MSDN Reference: http://msdn.microsoft.com/en-us/library/windows/desktop/dd390970
	/// </remarks>
	[StructLayout(LayoutKind.Sequential)]
	public struct WAVEFORMATEX
	{
		public short wFormatTag;
		public short nChannels;
		public int nSamplesPerSec;
		public int nAvgBytesPerSec;
		public short nBlockAlign;
		public short wBitsPerSample;
		public short cbSize;
	}
}