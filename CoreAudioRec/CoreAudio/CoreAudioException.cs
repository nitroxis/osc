﻿using System;
using System.Collections.Generic;

namespace CoreAudioRec
{
	public sealed class CoreAudioException : Exception
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new CoreAudioException.
		/// </summary>
		public CoreAudioException(uint errorCode)
			: base(getMessage(errorCode))
		{
		}

		#endregion

		#region Methods

		private static string getMessage(uint errorCode)
		{
			switch (errorCode)
			{
				case 0x88890001:
					return "Not initialized.";
				case 0x88890002:
					return "Already initialized.";
				case 0x88890003:
					return "Wrong endpoint type.";
				case 0x88890004:
					return "Device invalidated.";
				case 0x88890005:
					return "Not stopped.";
				case 0x88890006:
					return "Buffer too large.";
				case 0x88890007:
					return "Out of order.";
				case 0x88890008:
					return "Unsupported format.";
				case 0x88890009:
					return "Invalid size.";
				case 0x8889000a:
					return "Device in use.";
				case 0x8889000b:
					return "Buffer operation pending.";
				case 0x8889000c:
					return "Thread not registered.";
				case 0x8889000e:
					return "Exclusive mode not allowed.";
				case 0x8889000f:
					return "Endpoint create failed.";
				case 0x88890010:
					return "Service not running.";
				case 0x88890011:
					return "EventHandle not expected.";
				case 0x88890012:
					return "Exclusive mode only.";
				case 0x88890013:
					return "Buffer duration period not equal.";
				case 0x88890014:
					return "EventHandle not set.";
				case 0x88890015:
					return "Incorrect buffer size";
				case 0x88890016:
					return "Buffer size error.";
				case 0x88890017:
					return "CPU usage exceeded";
				case 0x88890018:
					return "Buffer error.";
				case 0x88890019:
					return "Buffer size not aligned.";
				case 0x88890020:
					return "Invalid device period.";
			}
			return "Unknown error.";
		}

		#endregion
	}
}
