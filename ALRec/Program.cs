﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;

namespace ALRec
{
	public static class Program
	{
		private static string[] formatNames =
		{
			"mono8", "mono16", "mono32f",
			"stereo8", "stereo16", "stereo32f",
		};

		private static ALFormat[] formats =
		{
			ALFormat.Mono8, ALFormat.Mono16, ALFormat.MonoFloat32Ext,
			ALFormat.Stereo8, ALFormat.Stereo16, ALFormat.StereoFloat32Ext,
		};

		private static int[] formatSizes =
		{
			1, 2, 4,
			2, 4, 8,
		};

		private static void printHelp()
		{
			Console.Error.WriteLine("Usage: alrec <format> <sampleRate> [deviceName]");
			Console.Error.WriteLine();

			Console.Error.WriteLine("Available formats:");
			for (int i = 0; i < formats.Length; i++)
				Console.Error.WriteLine("\t{0} ({1} bytes per sample)", formatNames[i], formatSizes[i]);
			Console.Error.WriteLine();

			Console.Error.WriteLine("Available devices:");
			for (int i = 0; i < AudioCapture.AvailableDevices.Count; i++)
				Console.Error.WriteLine("\t{0}", AudioCapture.AvailableDevices[i]);
			Console.Error.WriteLine();

			Console.Error.WriteLine("Default device (if none specified): {0}", AudioCapture.DefaultDevice);
		}

		public static int Main(string[] args)
		{		
			if (args.Length < 2)
			{
				printHelp();	
				return 1;
			}

			int formatSize = 0;
			ALFormat format = 0;
			for (int i = 0; i < formats.Length; i++)
			{
				if (formatNames[i] == args[0])
				{
					format = formats[i];
					formatSize = formatSizes[i];
					break;
				}
			}

			if (formatSize == 0)
			{
				Console.Error.WriteLine("Invalid format.");
				printHelp();
				return 1;
			}

			int sampleRate = int.Parse(args[1]);
			int bufferSize = sampleRate;
			string deviceName = args.Length >= 3 ? args[2] : AudioCapture.DefaultDevice;

			AudioContext alContext = new AudioContext();

			try
			{
				IntPtr device = Alc.CaptureOpenDevice(deviceName, sampleRate, format, bufferSize);
				if (device == IntPtr.Zero)
				{
					Console.Error.WriteLine("Could not open device.");
					return 1;
				}

				byte[] buffer = new byte[bufferSize * formatSize];
				GCHandle bufferHandle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
				IntPtr bufferPtr = bufferHandle.AddrOfPinnedObject();

				Alc.CaptureStart(device);

				Stream stdout = Console.OpenStandardOutput();

				bool run = true;
				Console.CancelKeyPress += (sender, e) => { run = false; };
				while (run)
				{
					int samples;
					Alc.GetInteger(device, AlcGetInteger.CaptureSamples, 4, out samples);

					if (samples <= 0)
					{
						Thread.Sleep(1);
						continue;
					}

					Alc.CaptureSamples(device, bufferPtr, samples); 
					stdout.Write(buffer, 0, samples * formatSize);
				}

				Alc.CaptureStop(device);
				Alc.CloseDevice(device);
				bufferHandle.Free();
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine(ex);
			}
			finally
			{
				alContext.Dispose();
			}

			return 0;
		}
	}
}
