#version 330

uniform int subdivisions;

layout(lines) in;
layout(points, max_vertices = 64) out;

in curveData
{
	vec2 prevPos;
	vec2 currentPos;
	vec2 nextPos;
} v[];

vec2 interpolate(vec2 v0, vec2 v1, vec2 v2, vec2 v3, float t)
{
	vec2 a = v3 - v2 - v0 + v1;
	vec2 b = v0 - v1 - a;
	vec2 c = v2 - v0;
	vec2 d = v1;

	return
		a * t * t * t +
		b * t * t +
		c * t +
		d;
}

void main(void)
{
	for(int i = 0; i < subdivisions; i++)
	{
		float t = float(i) / subdivisions;

		gl_Position = vec4(interpolate(
			v[0].prevPos,
			v[0].currentPos,
			v[1].currentPos,
			v[1].nextPos,
			t), 0.0, 1.0);

		EmitVertex();
		EndPrimitive();
	}
}

