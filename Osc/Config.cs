﻿using System;
using System.Windows.Forms;

namespace Osc
{
	public partial class Config : Form
	{
		public Config()
		{
			this.InitializeComponent();

			this.hueTrackBar.Value = (int)Properties.Settings.Default.Hue;
			this.saturationTrackBar.Value = (int)(Properties.Settings.Default.Saturation * this.saturationTrackBar.Maximum);
			this.valueTrackBar.Value = (int)(Properties.Settings.Default.Value * this.valueTrackBar.Maximum);
			this.subvisionsTrackBar.Value = Properties.Settings.Default.Subdivisions;
			this.fadeTrackBar.Value = (int)(Properties.Settings.Default.Fade * this.fadeTrackBar.Maximum);

			this.hueLabel.Text = string.Format("Hue:\n{0}°", this.hueTrackBar.Value);
			this.saturationLabel.Text = string.Format("Saturation:\n{0}%", this.saturationTrackBar.Value);
			this.valueLabel.Text = string.Format("Value:\n{0}%", this.valueTrackBar.Value);
			this.subdivisionsLabel.Text = string.Format("Subdivisions:\n{0}", this.subvisionsTrackBar.Value);
			this.fadeLabel.Text = string.Format("Fade:\n{0:0.0}%", (1.0d - Properties.Settings.Default.Fade) * 100.0d);
		}

		private void Config_FormClosing(object sender, FormClosingEventArgs e)
		{
			Properties.Settings.Default.Save();
		}

		private void hueTrackBar_Scroll(object sender, EventArgs e)
		{
			Properties.Settings.Default.Hue = this.hueTrackBar.Value;
			this.hueLabel.Text = string.Format("Hue:\n{0}°", this.hueTrackBar.Value);
		}

		private void saturationTrackBar_Scroll(object sender, EventArgs e)
		{
			Properties.Settings.Default.Saturation = (float)this.saturationTrackBar.Value / this.saturationTrackBar.Maximum;
			this.saturationLabel.Text = string.Format("Saturation:\n{0}%", this.saturationTrackBar.Value);
		}

		private void valueTrackBar_Scroll(object sender, EventArgs e)
		{
			Properties.Settings.Default.Value = (float)this.valueTrackBar.Value / this.valueTrackBar.Maximum;
			this.valueLabel.Text = string.Format("Value:\n{0}%", this.valueTrackBar.Value);
		}

		private void subvisionsTrackBar_Scroll(object sender, EventArgs e)
		{
			Properties.Settings.Default.Subdivisions = this.subvisionsTrackBar.Value;
			this.subdivisionsLabel.Text = string.Format("Subdivisions:\n{0}", this.subvisionsTrackBar.Value);
		}
		
		private void fadeTrackBar_Scroll(object sender, EventArgs e)
		{
			Properties.Settings.Default.Fade = (double)this.fadeTrackBar.Value / this.fadeTrackBar.Maximum;
			this.fadeLabel.Text = string.Format("Fade:\n{0:0.0}%", (1.0d - Properties.Settings.Default.Fade) * 100.0d);
		}
	}
}
