﻿using System;
using System.Threading;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Osc
{
	/// <summary>
	/// Represents an OpenGL framebuffer.
	/// </summary>
	public sealed class Framebuffer : IDisposable
	{
		#region Constants

		private const string vertexShader =
			"#version 330\n" +
			"layout(location = 0) in vec2 position;" +
			"out vec2 tc;" +
			"void main(void) { tc = 0.5 * (position + vec2(1.0)); gl_Position = vec4(position, 0.0, 1.0); }";

		private const string fragmentShader =
			"#version 330\n" +
			"uniform sampler2D tex;" +
			"in vec2 tc; out vec4 color;" +
			"void main(void) { color = texture(tex, tc); }";

		#endregion

		#region Fields

		private int width;
		private int height;

		private readonly int fbo;
		private readonly PixelInternalFormat format;
		private readonly Texture texture;
		private readonly GpuProgram program;

		private readonly int quadVertexArray;
		private readonly int quadVertexBuffer;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the current width of the frame buffer.
		/// </summary>
		public int Width
		{
			get { return this.width; }
		}

		/// <summary>
		/// Gets the current height of the frame buffer.
		/// </summary>
		public int Height
		{
			get { return this.height; }
		}

		/// <summary>
		/// Returns the texture render target.
		/// </summary>
		public Texture Texture
		{
			get { return this.texture; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Framebuffer.
		/// </summary>
		public Framebuffer(PixelInternalFormat format)
		{
			this.width = 1;
			this.height = 1;

			this.format = format;

			this.texture = new Texture("FBO texture");
			GL.BindTexture(TextureTarget.Texture2D, this.texture.ID); Helper.CheckGLError();
			GL.TexImage2D(TextureTarget.Texture2D, 0, this.format, this.width, this.height, 0, PixelFormat.Rgb, PixelType.UnsignedByte, IntPtr.Zero); Helper.CheckGLError();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest); Helper.CheckGLError();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest); Helper.CheckGLError();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge); Helper.CheckGLError();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge); Helper.CheckGLError();

			this.fbo = GL.GenFramebuffer(); Helper.CheckGLError();
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, this.fbo); Helper.CheckGLError();
			GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, this.texture.ID, 0); Helper.CheckGLError();
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0); Helper.CheckGLError();
			GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);

			this.program = new GpuProgram("Shadow FBO");
			this.program.Attach(ShaderType.VertexShader, vertexShader);
			this.program.Attach(ShaderType.FragmentShader, fragmentShader);
			this.program.Link();

			this.quadVertexBuffer = GL.GenBuffer(); Helper.CheckGLError();
			this.quadVertexArray = GL.GenVertexArray(); Helper.CheckGLError();
			GL.BindVertexArray(this.quadVertexArray); Helper.CheckGLError();
			GL.BindBuffer(BufferTarget.ArrayBuffer, this.quadVertexBuffer); Helper.CheckGLError();
			GL.EnableVertexAttribArray(0); Helper.CheckGLError();
			GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, 0, 0); Helper.CheckGLError();

			float[] data =
			{
				+1.0f, +1.0f,
				-1.0f, +1.0f,
				-1.0f, -1.0f,
				+1.0f, -1.0f
			};

			GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(32), data, BufferUsageHint.StaticDraw); Helper.CheckGLError();
		}

		public Framebuffer(int id)
		{		
			this.fbo = id;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Binds the frame buffer.
		/// </summary>
		public void Bind()
		{
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, this.fbo); Helper.CheckGLError();
			GL.Viewport(0, 0, this.width, this.height); Helper.CheckGLError();
		}

		/// <summary>
		/// Binds the frame buffer and clears it with the specified color.
		/// </summary>
		/// <param name="color"></param>
		public void BindAndClear(Color4 color)
		{
			this.Bind();
			GL.ClearColor(color); Helper.CheckGLError();
			GL.Clear(ClearBufferMask.ColorBufferBit); Helper.CheckGLError();
		}

		/// <summary>
		/// Sets the size of the frame buffer.
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		public void SetSize(int width, int height)
		{
			this.width = width;
			this.height = height;

			if (this.texture != null)
			{
				GL.BindTexture(TextureTarget.Texture2D, this.texture.ID); Helper.CheckGLError();
				GL.TexImage2D(TextureTarget.Texture2D, 0, this.format, this.width, this.height, 0, PixelFormat.Rgb, PixelType.UnsignedByte, IntPtr.Zero); Helper.CheckGLError();			
			}
		}

		/// <summary>
		/// Shows the frame buffer on a full screen quad.
		/// </summary>
		public void Show()
		{
			if (this.texture == null)
				return;

			// render to screen.
			this.program.Use();
			GL.Uniform1(this.program["tex"], 0); Helper.CheckGLError();
		
			GL.BindTexture(TextureTarget.Texture2D, this.texture.ID);
			GL.BindVertexArray(this.quadVertexArray); Helper.CheckGLError();
			GL.DrawArrays(PrimitiveType.TriangleFan, 0, 4); Helper.CheckGLError();
		}

		public void ShowWithoutProgram()
		{
			if (this.texture == null)
				return;

			GL.BindTexture(TextureTarget.Texture2D, this.texture.ID);
			GL.BindVertexArray(this.quadVertexArray); Helper.CheckGLError();
			GL.DrawArrays(PrimitiveType.TriangleFan, 0, 4); Helper.CheckGLError();
		}

		public void Dispose()
		{
			if (this.fbo != 0)
			{
				GL.DeleteFramebuffer(this.fbo); Helper.CheckGLError();
			}

			if (this.quadVertexArray != 0)
			{
				GL.DeleteVertexArray(this.quadVertexArray); Helper.CheckGLError();
			}

			if (this.quadVertexBuffer != 0)
			{
				GL.DeleteBuffer(this.quadVertexBuffer); Helper.CheckGLError();
			}

			if (this.texture != null)
				this.texture.Dispose();
			
			if(this.program != null)
				this.program.Dispose();
		}

		#endregion
	}
}
