#version 330

uniform float alpha;

out vec4 color;

void main(void)
{
	color = vec4(alpha, alpha, alpha, 1.0);
}