﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;

namespace Osc
{
	/// <summary>
	/// Represents a texture.
	/// </summary>
	public sealed class Texture : IDisposable
	{
		#region Fields

		private readonly string name; 
		private readonly int id;

		private int width;
		private int height;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the texture.
		/// </summary>
		public string Name
		{
			get { return this.name; }
		}

		/// <summary>
		/// Gets the OpenGL ID.
		/// </summary>
		internal int ID
		{
			get { return this.id; }
		}

		/// <summary>
		/// Gets or sets the width of the texture.
		/// </summary>
		public int Width
		{
			get { return this.width; }
			set { this.width = value; }
		}

		/// <summary>
		/// Gets or sets the height of the texture.
		/// </summary>
		public int Height
		{
			get { return this.height; }
			set { this.height = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Texture.
		/// </summary>
		public Texture(string name)
		{
			this.name = name; 

			this.id = GL.GenTexture(); Helper.CheckGLError();
		}

		/// <summary>
		/// Creates a new Texture from a Bitmap.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="bitmap"></param>
		public Texture(string name, Bitmap bitmap)
			: this(name)
		{
			this.FromBitmap(bitmap);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Disposes the texture and deletes the underlying OpenGL object.
		/// </summary>
		public void Dispose()
		{
			GL.DeleteTexture(this.id);
			Helper.CheckGLError();
		}

		/// <summary>
		/// Updates the size properties of the texture.
		/// </summary>
		public void UpdateSize()
		{
			GL.BindTexture(TextureTarget.Texture2D, this.id); Helper.CheckGLError();
			GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureWidth, out this.width); Helper.CheckGLError();
			GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureHeight, out this.height); Helper.CheckGLError();
		}

		/// <summary>
		/// Sets the texture filtering mode.
		/// </summary>
		/// <param name="linear"></param>
		public void SetFilter(bool linear)
		{
			GL.BindTexture(TextureTarget.Texture2D, this.id); Helper.CheckGLError();
			if (linear)
			{
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear); Helper.CheckGLError();
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear); Helper.CheckGLError();
			}
			else
			{
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest); Helper.CheckGLError();
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest); Helper.CheckGLError();
			}
		}

		/// <summary>
		/// Loads a texture from a bitmap.
		/// </summary>
		/// <param name="bitmap"></param>
		public void FromBitmap(Bitmap bitmap)
		{
			this.width = bitmap.Width;
			this.height = bitmap.Height;

			GL.BindTexture(TextureTarget.Texture2D, this.id); Helper.CheckGLError();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest); Helper.CheckGLError();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest); Helper.CheckGLError();

			BitmapData data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			GL.PixelStore(PixelStoreParameter.UnpackRowLength, data.Stride / 4);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bitmap.Width, bitmap.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0); Helper.CheckGLError();
			bitmap.UnlockBits(data);
		}
		
		/// <summary>
		/// Converts the texture to a bitmap.
		/// </summary>
		/// <returns></returns>
		public Bitmap ToBitmap()
		{
			GL.BindTexture(TextureTarget.Texture2D, this.id); Helper.CheckGLError();
			int width, height;
			GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureWidth, out width); Helper.CheckGLError();
			GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureHeight, out height); Helper.CheckGLError();
			
			Bitmap bmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			BitmapData data = bmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

			GL.GetTexImage(TextureTarget.Texture2D, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);

			bmp.UnlockBits(data);
			return bmp;
		}

		#endregion
	}
}
