﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace Osc
{
	/// <summary>
	/// Represents the main window.
	/// </summary>
	public sealed class MainWindow : GameWindow
	{
		#region Fields

		private readonly Stream inputStream;
		private readonly byte[] buffer;
		private readonly int bytesPerFrame;
		
		private readonly Framebuffer outputFBO;
		private readonly Framebuffer oscFBO;

		private readonly GpuProgram oscProgram;
		private readonly int oscVertexBuffer;
		private readonly int oscVertexArray;

		private readonly GpuProgram clearProgram;
		private readonly int quadVertexArray;
		private readonly int quadVertexBuffer;

		private double gain;
		private int frames;

		private readonly Stopwatch fpsWatch;
		private int fpsCounter;

		private Config config;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new MainWindow.
		/// </summary>
		public MainWindow(Stream inputStream, VertexAttribPointerType inputType)
			: base(512, 512, GraphicsMode.Default, "Osc", GameWindowFlags.Default, DisplayDevice.Default, 3, 3, GraphicsContextFlags.ForwardCompatible)
		{
			this.inputStream = inputStream;
	
			this.outputFBO = new Framebuffer(0);
			this.oscFBO = new Framebuffer(PixelInternalFormat.Rgba32f);

			this.VSync = VSyncMode.On;

			int bytesPerSample;
			if (inputType == VertexAttribPointerType.Byte) bytesPerSample = 1;
			else if (inputType == VertexAttribPointerType.Double) bytesPerSample = 8;
			else if (inputType == VertexAttribPointerType.Float) bytesPerSample = 4;
			else if (inputType == VertexAttribPointerType.HalfFloat) bytesPerSample = 2;
			else if (inputType == VertexAttribPointerType.Int) bytesPerSample = 4;
			else if (inputType == VertexAttribPointerType.Short) bytesPerSample = 2;
			else if (inputType == VertexAttribPointerType.UnsignedByte) bytesPerSample = 1;
			else if (inputType == VertexAttribPointerType.UnsignedInt) bytesPerSample = 4;
			else if (inputType == VertexAttribPointerType.UnsignedShort) bytesPerSample = 2;
			else throw new NotSupportedException(string.Format("{0} is not supported.", inputType));

			this.bytesPerFrame = bytesPerSample * 2;
			this.buffer = new byte[2048 * this.bytesPerFrame];

			// create osc stuff.
			{
				this.oscProgram = new GpuProgram("Osc");
				this.oscProgram.Attach(ShaderType.VertexShader, Encoding.UTF8.GetString(Properties.Resources.OscVertex));
				this.oscProgram.Attach(ShaderType.GeometryShader, Encoding.UTF8.GetString(Properties.Resources.OscGeometry));
				this.oscProgram.Attach(ShaderType.FragmentShader, Encoding.UTF8.GetString(Properties.Resources.OscFragment));
				this.oscProgram.Link();

				this.oscVertexArray = GL.GenVertexArray(); Helper.CheckGLError();
				this.oscVertexBuffer = GL.GenBuffer(); Helper.CheckGLError();
				GL.BindVertexArray(this.oscVertexArray); Helper.CheckGLError();
				
				GL.BindBuffer(BufferTarget.ArrayBuffer, this.oscVertexBuffer); Helper.CheckGLError();
				GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(this.buffer.Length + this.bytesPerFrame * 2), IntPtr.Zero, BufferUsageHint.StreamDraw);

				// prev
				GL.EnableVertexAttribArray(0); Helper.CheckGLError();
				GL.VertexAttribPointer(0, 2, inputType, true, 0, this.bytesPerFrame * 0); Helper.CheckGLError();
				
				// current
				GL.EnableVertexAttribArray(1); Helper.CheckGLError();
				GL.VertexAttribPointer(1, 2, inputType, true, 0, this.bytesPerFrame * 1); Helper.CheckGLError();
				
				// next
				GL.EnableVertexAttribArray(2); Helper.CheckGLError();
				GL.VertexAttribPointer(2, 2, inputType, true, 0, this.bytesPerFrame * 2); Helper.CheckGLError();
			}

			// create fullscreen quad and program for fade effect.
			{
				this.quadVertexBuffer = GL.GenBuffer(); Helper.CheckGLError();
				this.quadVertexArray = GL.GenVertexArray(); Helper.CheckGLError();

				GL.BindVertexArray(this.quadVertexArray); Helper.CheckGLError();
				
				GL.BindBuffer(BufferTarget.ArrayBuffer, this.quadVertexBuffer); Helper.CheckGLError();
				GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(32), new[] {+1.0f, +1.0f, -1.0f, +1.0f, -1.0f, -1.0f, +1.0f, -1.0f}, BufferUsageHint.StaticDraw); Helper.CheckGLError();
				GL.EnableVertexAttribArray(0); Helper.CheckGLError();
				GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, 0, 0); Helper.CheckGLError();

				this.clearProgram = new GpuProgram("Clear");
				this.clearProgram.Attach(ShaderType.VertexShader, Encoding.UTF8.GetString(Properties.Resources.ClearVertex));
				this.clearProgram.Attach(ShaderType.FragmentShader, Encoding.UTF8.GetString(Properties.Resources.ClearFragment));
				this.clearProgram.Link();
			}

			this.gain = 1.0d;

			this.fpsWatch = new Stopwatch();
			this.fpsWatch.Start();
		}
		
		#endregion

		#region Methods

		protected override void OnRenderFrame(FrameEventArgs e)
		{		
			base.OnRenderFrame(e);

			// set sizes.
			if ((this.ClientSize.Width != this.outputFBO.Width && this.ClientSize.Width != 0) || (this.ClientSize.Height != this.outputFBO.Height && this.ClientSize.Height != 0))
			{
				this.outputFBO.SetSize(this.ClientSize.Width, this.ClientSize.Height);
				this.oscFBO.SetSize(this.ClientSize.Width, this.ClientSize.Height);
			}

			GL.BindBuffer(BufferTarget.ArrayBuffer, this.oscVertexBuffer); Helper.CheckGLError();

			if (this.frames > 0)
			{
				// interpolation start point is the last point of the old buffer.s
				GL.CopyBufferSubData(BufferTarget.ArrayBuffer, BufferTarget.ArrayBuffer, new IntPtr(frames * this.bytesPerFrame), IntPtr.Zero, new IntPtr(this.bytesPerFrame)); Helper.CheckGLError();
			}

			// read from input stream.
			int bytesRead = this.inputStream.Read(this.buffer, 0, this.buffer.Length);
			if (bytesRead == 0)
			{
				this.Close();
				return;
			}

			// in case we didn't read a full frame...
			while (bytesRead % this.bytesPerFrame != 0)
				bytesRead += this.inputStream.Read(this.buffer, bytesRead, this.bytesPerFrame - (bytesRead % this.bytesPerFrame));

			this.frames = bytesRead / this.bytesPerFrame;
			
			// upload data to GPU.
			GL.BufferSubData(BufferTarget.ArrayBuffer, new IntPtr(this.bytesPerFrame), new IntPtr(bytesRead), this.buffer); Helper.CheckGLError();
			
			// fix interpolation end point.
			GL.CopyBufferSubData(BufferTarget.ArrayBuffer, BufferTarget.ArrayBuffer, new IntPtr(frames * this.bytesPerFrame), new IntPtr((frames + 1) * this.bytesPerFrame), new IntPtr(this.bytesPerFrame)); Helper.CheckGLError();

			// use FBO.
			this.oscFBO.Bind();
			GL.Enable(EnableCap.Blend); Helper.CheckGLError();

			// fade screen.
			this.clearProgram.Use();
			GL.Uniform1(this.clearProgram["alpha"], (float)Math.Pow(Properties.Settings.Default.Fade, e.Time * 60.0d)); Helper.CheckGLError();
			GL.BlendFunc(BlendingFactorSrc.DstColor, BlendingFactorDest.Zero); Helper.CheckGLError();
			GL.BindVertexArray(this.quadVertexArray); Helper.CheckGLError();
			GL.DrawArrays(PrimitiveType.TriangleFan, 0, 4); Helper.CheckGLError();

			float aspectX;
			float aspectY;

			if (this.ClientSize.Width < this.ClientSize.Height)
			{
				aspectX = 1.0f;
				aspectY = (float)this.ClientSize.Width / (float)this.ClientSize.Height;
			}
			else
			{
				aspectX = (float)this.ClientSize.Height / (float)this.ClientSize.Width;
				aspectY = 1.0f;
			}

			Vector3 color;
			hsv2rgb(Properties.Settings.Default.Hue, Properties.Settings.Default.Saturation, Properties.Settings.Default.Value, out color.X, out color.Y, out color.Z);
			
			// draw osc points.
			this.oscProgram.Use();
			GL.Uniform2(this.oscProgram["scale"], (float)this.gain * aspectX, (float)this.gain * aspectY); Helper.CheckGLError();
			GL.Uniform1(this.oscProgram["subdivisions"], Properties.Settings.Default.Subdivisions); Helper.CheckGLError();
			GL.Uniform3(this.oscProgram["color"], ref color); Helper.CheckGLError();
			GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One); Helper.CheckGLError();
			GL.BindVertexArray(this.oscVertexArray); Helper.CheckGLError();
			GL.DrawArrays(PrimitiveType.LineStrip, 0, this.frames); Helper.CheckGLError();

			// draw to screen.
			this.outputFBO.BindAndClear(Color4.Black);
			this.oscFBO.Show();
			this.SwapBuffers();

			if (this.fpsWatch.Elapsed.TotalSeconds >= 1.0d)
			{
				this.fpsWatch.Restart();
				this.Title = string.Format("Osc - {0} FPS - VSync {1}", this.fpsCounter - 1, this.VSync);
				this.fpsCounter = 0;
			}
			this.fpsCounter++;
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);

			Console.WriteLine("{0}x{1}", this.ClientSize.Width, this.ClientSize.Height);
		}

		protected override void OnMouseWheel(MouseWheelEventArgs e)
		{
			base.OnMouseWheel(e);

			this.gain *= Math.Pow(0.9d, -e.DeltaPrecise);
			Console.Error.WriteLine("Gain: {0:0.0000}", this.gain);
		}

		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			base.OnKeyPress(e);

			if (e.KeyChar == 'v')
			{
				if (this.VSync == VSyncMode.On)
					this.VSync = VSyncMode.Off;
				else
					this.VSync = VSyncMode.On;
			}
			else if (e.KeyChar == ' ')
			{
				if (this.config == null || this.config.IsDisposed)
					this.config = new Config();

				this.config.Show();
			}
		}

		protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
		{
			base.OnClosing(e);

			GL.DeleteVertexArray(this.oscVertexArray); Helper.CheckGLError();
			GL.DeleteBuffer(this.oscVertexBuffer); Helper.CheckGLError();

			GL.DeleteVertexArray(this.quadVertexArray); Helper.CheckGLError();
			GL.DeleteBuffer(this.quadVertexBuffer); Helper.CheckGLError();
			
			this.oscProgram.Dispose();

			this.outputFBO.Dispose();
			this.oscFBO.Dispose();
		}

		private static void hsv2rgb(float h, float s, float v, out float r, out float g, out float b)
		{
			// http://www.cs.rit.edu/~ncs/color/t_convert.html

			int i;
			float f, p, q, t;

			if (s <= float.Epsilon)
			{
				r = g = b = v;
				return;
			}

			h = (h / 60.0f) % 6.0f;
			i = (int)h;
			f = h - i;
			p = v * (1.0f - s);
			q = v * (1.0f - s * f);
			t = v * (1.0f - s * (1.0f - f));

			switch (i)
			{
				case 0: r = v; g = t; b = p; break;
				case 1: r = q; g = v; b = p; break;
				case 2: r = p; g = v; b = t; break;
				case 3: r = p; g = q; b = v; break;
				case 4: r = t; g = p; b = v; break;
				default: r = v; g = p; b = q; break;
			}
		}

		#endregion
	}
}

