﻿using System;
using System.Collections.Generic;
using System.Threading;
using OpenTK.Graphics.OpenGL;

namespace Osc
{
	/// <summary>
	/// Represents an OpenGL GPU program.
	/// </summary>
	public sealed class GpuProgram : IDisposable
	{
		#region Fields
		
		private readonly string name;
		private readonly int program;
		private readonly List<int> shaders;
		private readonly Dictionary<string, int> uniformCache;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the program.
		/// </summary>
		public string Name
		{
			get { return this.name; }
		}

		/// <summary>
		/// Retreives the location of a uniform.
		/// </summary>
		/// <param name="varName"></param>
		/// <returns></returns>
		public int this[string varName]
		{
			get
			{
				int location;
				if (this.uniformCache.TryGetValue(varName, out location))
					return location;

				location = GL.GetUniformLocation(this.program, varName);
				Helper.CheckGLError();

				if (location < 0)
					Console.WriteLine("{0}: Could not find uniform \"{1}\".", this.name, varName);

				this.uniformCache.Add(varName, location);
				return location;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new GpuProgram.
		/// </summary>
		public GpuProgram(string name)
		{
			this.name = name;
			this.program = GL.CreateProgram(); Helper.CheckGLError();

			this.shaders = new List<int>();
			this.uniformCache = new Dictionary<string, int>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Attaches a shader.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="sources"></param>
		public void Attach(ShaderType type, params string[] sources)
		{
			int shader = GL.CreateShader(type);
			Helper.CheckGLError();

			int[] sourceLengths = new int[sources.Length];
			for (int i = 0; i < sources.Length; i++)
				sourceLengths[i] = sources[i].Length;

			GL.ShaderSource(shader, sources.Length, sources, sourceLengths);
			Helper.CheckGLError();

			GL.CompileShader(shader);
			Helper.CheckGLError();

			string log = GL.GetShaderInfoLog(shader);
			Helper.CheckGLError();

			if (!string.IsNullOrWhiteSpace(log))
				Console.WriteLine("{0}.{1} info log: {2}", this.name, type, log);

			int status;
			GL.GetShader(shader, ShaderParameter.CompileStatus, out status);
			Helper.CheckGLError();
			if (status == 0)
				throw new Exception(log);

			GL.AttachShader(program, shader);
			Helper.CheckGLError();

			this.shaders.Add(shader);
		}

		/// <summary>
		/// Associates a generic vertex attribute index with a named attribute variable.
		/// </summary>
		/// <param name="index"></param>
		/// <param name="name"></param>
		public void BindAttribute(int index, string name)
		{
			GL.BindAttribLocation(this.program, index, name);
			Helper.CheckGLError();
		}

		/// <summary>
		/// Links the program.
		/// </summary>
		public void Link()
		{
			GL.LinkProgram(program);
			Helper.CheckGLError();

			string log = GL.GetProgramInfoLog(program);
			Helper.CheckGLError();
			if (!string.IsNullOrWhiteSpace(log))
				Console.WriteLine("{0} info log: {1}", this.name, log);

			int status;
			GL.GetProgram(program, GetProgramParameterName.LinkStatus, out status);
			Helper.CheckGLError();
			if (status == 0)
				throw new Exception(log);
		}

		/// <summary>
		/// Uses the program.
		/// </summary>
		public void Use()
		{
			GL.UseProgram(this.program);
			Helper.CheckGLError();
		}

		/// <summary>
		/// Deletes the program.
		/// </summary>
		public void Dispose()
		{
			GL.DeleteProgram(this.program);
			Helper.CheckGLError();
			foreach (int shader in this.shaders)
			{
				GL.DeleteShader(shader);
				Helper.CheckGLError();
			}
		}

		#endregion
	}
}
