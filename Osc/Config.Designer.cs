﻿namespace Osc
{
	partial class Config
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.hueTrackBar = new System.Windows.Forms.TrackBar();
			this.hueLabel = new System.Windows.Forms.Label();
			this.saturationLabel = new System.Windows.Forms.Label();
			this.saturationTrackBar = new System.Windows.Forms.TrackBar();
			this.valueLabel = new System.Windows.Forms.Label();
			this.valueTrackBar = new System.Windows.Forms.TrackBar();
			this.subdivisionsLabel = new System.Windows.Forms.Label();
			this.subvisionsTrackBar = new System.Windows.Forms.TrackBar();
			this.fadeLabel = new System.Windows.Forms.Label();
			this.fadeTrackBar = new System.Windows.Forms.TrackBar();
			((System.ComponentModel.ISupportInitialize)(this.hueTrackBar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.saturationTrackBar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.valueTrackBar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.subvisionsTrackBar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fadeTrackBar)).BeginInit();
			this.SuspendLayout();
			// 
			// hueTrackBar
			// 
			this.hueTrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.hueTrackBar.Location = new System.Drawing.Point(106, 12);
			this.hueTrackBar.Maximum = 360;
			this.hueTrackBar.Name = "hueTrackBar";
			this.hueTrackBar.Size = new System.Drawing.Size(515, 45);
			this.hueTrackBar.TabIndex = 0;
			this.hueTrackBar.TickFrequency = 60;
			this.hueTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
			this.hueTrackBar.Scroll += new System.EventHandler(this.hueTrackBar_Scroll);
			// 
			// hueLabel
			// 
			this.hueLabel.Location = new System.Drawing.Point(12, 12);
			this.hueLabel.Name = "hueLabel";
			this.hueLabel.Size = new System.Drawing.Size(88, 45);
			this.hueLabel.TabIndex = 1;
			this.hueLabel.Text = "Hue:";
			this.hueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// saturationLabel
			// 
			this.saturationLabel.Location = new System.Drawing.Point(12, 63);
			this.saturationLabel.Name = "saturationLabel";
			this.saturationLabel.Size = new System.Drawing.Size(88, 45);
			this.saturationLabel.TabIndex = 3;
			this.saturationLabel.Text = "Saturation:";
			this.saturationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// saturationTrackBar
			// 
			this.saturationTrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.saturationTrackBar.Location = new System.Drawing.Point(106, 63);
			this.saturationTrackBar.Maximum = 100;
			this.saturationTrackBar.Name = "saturationTrackBar";
			this.saturationTrackBar.Size = new System.Drawing.Size(515, 45);
			this.saturationTrackBar.TabIndex = 2;
			this.saturationTrackBar.TickFrequency = 10;
			this.saturationTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
			this.saturationTrackBar.Scroll += new System.EventHandler(this.saturationTrackBar_Scroll);
			// 
			// valueLabel
			// 
			this.valueLabel.Location = new System.Drawing.Point(12, 114);
			this.valueLabel.Name = "valueLabel";
			this.valueLabel.Size = new System.Drawing.Size(88, 45);
			this.valueLabel.TabIndex = 5;
			this.valueLabel.Text = "Value:";
			this.valueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// valueTrackBar
			// 
			this.valueTrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.valueTrackBar.Location = new System.Drawing.Point(106, 114);
			this.valueTrackBar.Maximum = 100;
			this.valueTrackBar.Name = "valueTrackBar";
			this.valueTrackBar.Size = new System.Drawing.Size(515, 45);
			this.valueTrackBar.TabIndex = 4;
			this.valueTrackBar.TickFrequency = 10;
			this.valueTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
			this.valueTrackBar.Scroll += new System.EventHandler(this.valueTrackBar_Scroll);
			// 
			// subdivisionsLabel
			// 
			this.subdivisionsLabel.Location = new System.Drawing.Point(12, 165);
			this.subdivisionsLabel.Name = "subdivisionsLabel";
			this.subdivisionsLabel.Size = new System.Drawing.Size(88, 45);
			this.subdivisionsLabel.TabIndex = 7;
			this.subdivisionsLabel.Text = "Subdivisions:";
			this.subdivisionsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// subvisionsTrackBar
			// 
			this.subvisionsTrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.subvisionsTrackBar.Location = new System.Drawing.Point(106, 165);
			this.subvisionsTrackBar.Maximum = 64;
			this.subvisionsTrackBar.Minimum = 1;
			this.subvisionsTrackBar.Name = "subvisionsTrackBar";
			this.subvisionsTrackBar.Size = new System.Drawing.Size(515, 45);
			this.subvisionsTrackBar.TabIndex = 6;
			this.subvisionsTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
			this.subvisionsTrackBar.Value = 1;
			this.subvisionsTrackBar.Scroll += new System.EventHandler(this.subvisionsTrackBar_Scroll);
			// 
			// fadeLabel
			// 
			this.fadeLabel.Location = new System.Drawing.Point(12, 216);
			this.fadeLabel.Name = "fadeLabel";
			this.fadeLabel.Size = new System.Drawing.Size(88, 45);
			this.fadeLabel.TabIndex = 9;
			this.fadeLabel.Text = "Fade:";
			this.fadeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// fadeTrackBar
			// 
			this.fadeTrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.fadeTrackBar.Location = new System.Drawing.Point(106, 216);
			this.fadeTrackBar.Maximum = 1000;
			this.fadeTrackBar.Name = "fadeTrackBar";
			this.fadeTrackBar.Size = new System.Drawing.Size(515, 45);
			this.fadeTrackBar.TabIndex = 8;
			this.fadeTrackBar.TickFrequency = 50;
			this.fadeTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
			this.fadeTrackBar.Scroll += new System.EventHandler(this.fadeTrackBar_Scroll);
			// 
			// Config
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(633, 273);
			this.Controls.Add(this.fadeLabel);
			this.Controls.Add(this.fadeTrackBar);
			this.Controls.Add(this.subdivisionsLabel);
			this.Controls.Add(this.subvisionsTrackBar);
			this.Controls.Add(this.valueLabel);
			this.Controls.Add(this.valueTrackBar);
			this.Controls.Add(this.saturationLabel);
			this.Controls.Add(this.saturationTrackBar);
			this.Controls.Add(this.hueLabel);
			this.Controls.Add(this.hueTrackBar);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Config";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Config";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Config_FormClosing);
			((System.ComponentModel.ISupportInitialize)(this.hueTrackBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.saturationTrackBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.valueTrackBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.subvisionsTrackBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fadeTrackBar)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TrackBar hueTrackBar;
		private System.Windows.Forms.Label hueLabel;
		private System.Windows.Forms.Label saturationLabel;
		private System.Windows.Forms.TrackBar saturationTrackBar;
		private System.Windows.Forms.Label valueLabel;
		private System.Windows.Forms.TrackBar valueTrackBar;
		private System.Windows.Forms.Label subdivisionsLabel;
		private System.Windows.Forms.TrackBar subvisionsTrackBar;
		private System.Windows.Forms.Label fadeLabel;
		private System.Windows.Forms.TrackBar fadeTrackBar;
	}
}