#version 330

uniform vec2 scale;

layout(location = 0) in vec2 prev;
layout(location = 1) in vec2 current;
layout(location = 2) in vec2 next;

out curveData
{
	vec2 prevPos;
	vec2 currentPos;
	vec2 nextPos;
};

void main(void)
{
	prevPos = prev * scale;
	currentPos = current * scale;
	nextPos = next * scale;
}