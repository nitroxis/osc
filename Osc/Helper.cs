﻿using System;
using OpenTK.Graphics.OpenGL;

namespace Osc
{
	/// <summary>
	/// OpenGL helper methods.
	/// </summary>
	public static class Helper
	{
		#region Methods

		public static void CheckGLError()
		{
			ErrorCode code = GL.GetError();
			if (code == ErrorCode.NoError)
				return;

			Console.WriteLine("OpenGL error: {0}", code);

			throw new Exception(code.ToString());
		}

		#endregion
	}
}
