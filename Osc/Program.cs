﻿using System;
using OpenTK.Graphics.OpenGL;

namespace Osc
{
	public static class Program
	{
		public static int Main(string[] args)
		{
			VertexAttribPointerType inputType = VertexAttribPointerType.Short;

			if (args.Length >= 1)
			{
				if (!Enum.TryParse(args[0], true, out inputType))
				{
					Console.Error.WriteLine("Invalid data type.");
					Console.Error.WriteLine("Available data types:");
					foreach(string name in Enum.GetNames(typeof(VertexAttribPointerType)))
						Console.WriteLine("\t{0}", name);

					return 1;
				}
			}

			MainWindow window = new MainWindow(Console.OpenStandardInput(), inputType);
			window.Run();

			return 0;
		}
	}
}
